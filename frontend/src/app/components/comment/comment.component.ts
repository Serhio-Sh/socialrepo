import { Component, Input } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { LikeService } from '../../services/like.service';
import { User } from '../../models/user';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { AuthDialogService } from '../../services/auth-dialog.service';

import { AuthenticationService } from '../../services/auth.service';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,

        private likeService: LikeService,
    ) { }

    private unsubscribe$ = new Subject<void>();


    public CounterlikeComment() {
        return this.comment.reactions.filter((x) => x.isLike == true).length
    }

    public CounterDislikeComment() {
        return this.comment.reactions.filter((x) => x.isLike == false).length
    }

    public likeComment() {
        if (!this.currentUser) {

            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
