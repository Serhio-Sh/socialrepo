import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';

import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { CommentService } from './comment.service';

import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }

    public likePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: true,
            userId: currentUser.id
        };


        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        if (hasReaction) {
            if (post.reactions.some((x) => x.user.id == currentUser.id && x.isLike === true)) {
                post.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                post.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: true, user: currentUser });
            }
        }
        else {
            post.reactions = innerPost.reactions.concat({ isLike: true, user: currentUser });
        }

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: true, user: currentUser });

                return of(innerPost);
            })
        );
    }

    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        if (hasReaction) {
            if (post.reactions.some((x) => x.user.id == currentUser.id && x.isLike === false)) {
                post.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                post.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: false, user: currentUser });
            }
        }
        else {
            post.reactions = innerPost.reactions.concat({ isLike: false, user: currentUser });
        }

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: false, user: currentUser });

                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: true,
            userId: currentUser.id
        };

        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);

        if (hasReaction) {
            if (comment.reactions.some((x) => x.user.id == currentUser.id && x.isLike === true)) {
                comment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                comment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: true, user: currentUser });
            }
        }
        else {
            comment.reactions = innerComment.reactions.concat({ isLike: true, user: currentUser });
        }

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = hasReaction
                    ? innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerComment.reactions.concat({ isLike: true, user: currentUser });

                return of(innerComment);
            })
        );

    }

    public dislikeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);

        if (hasReaction) {
            if (comment.reactions.some((x) => x.user.id == currentUser.id && x.isLike === false)) {
                comment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
            }
            else {
                comment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id).concat({ isLike: false, user: currentUser });
            }
        }
        else {
            comment.reactions = innerComment.reactions.concat({ isLike: false, user: currentUser });
        }

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = hasReaction
                    ? innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerComment.reactions.concat({ isLike: false, user: currentUser });

                return of(innerComment);
            })
        );
    }
}
