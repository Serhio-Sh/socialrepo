import { Component } from '@angular/core';

@Component({    //Декоратор 
    selector: 'app-root',
    templateUrl: './app.component.html', //кусок разметки HTML
    styleUrls: ['./app.component.sass']
})
export class AppComponent { //Компонент
    public title = 'thread-net';
}
